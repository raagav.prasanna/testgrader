// Shay Alex Lelichev 2043812
package fileGenerators;

public class Main {

    public static void main(String[] args) {

        QuestionGenerator qg = new QuestionGenerator(2, 2, 6);
        qg.generateQuestions();
        StudentGenerator sg = new StudentGenerator();
        sg.generateStudents();
    }
}