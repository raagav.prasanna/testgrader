// Shay Alex Lelichev 2043812
package fileGenerators;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.util.Random;
import java.nio.file.*;
import java.util.List;

public class QuestionGenerator {
    
    private int tF;
    private int multi;
    private int shortAns;
    private File qF;
    private Random r;

    //creates the QuestionGenerator object with file paths and a random.
    public QuestionGenerator(int tF , int multi , int shortAns) {
        if (tF + multi + shortAns != 10) {
            throw new IllegalArgumentException("There must be only 10 questions.");
        }
        this.tF = tF;
        this.multi = multi;
        this.shortAns = shortAns;
        //path to the generated file questions.txt
        this.qF = new File("txtfiles/questions.txt");
        this.r = new Random();
    }

    //This method generates the questions using our format with the id of the question in the front and the question itself following it.
    public void generateQuestions() {
        try {
            FileWriter fw = new FileWriter(this.qF);
            for (int i = 0; i < tF + multi + shortAns; i++) {
                if (i == 0) {
                    fw.write("00" + (i + 1) + ";" + selectRandomQuestion());
                } else if (i == 9) {
                    fw.write("\n0" + (i + 1) + ";" + selectRandomQuestion());
                } else {
                    fw.write("\n00" + (i + 1) + ";" + selectRandomQuestion());
                }
            }
            fw.close();
        } catch (IOException e) {
            System.out.println("There was an error");
        }
    }

    //randomly selects a true/false question to import from the trueFalse.txt file.
    public String createTF() {
        List<String> tf = null;
        try {
            tf = Files.readAllLines(Paths.get("fileGenerators/trueFalse.txt"));
        } catch (IOException e) {
            System.out.println("The true/false file was not found.");
        }
        return tf.get(this.r.nextInt(34));
    }
    
    //randomly selects a short answer question to import from the shortAnswer.txt file.
    public String createShortAns() {
        List<String> sa = null;
        try {
            sa = Files.readAllLines(Paths.get("fileGenerators/shortAnswer.txt"));
        } catch (IOException e) {
            System.out.println("The short answer file was not found.");
        }
        return sa.get(this.r.nextInt(19));
    }

    /*This method uses a couple of randomizers to select some numeric values and select the type of question to put them in
    the question can be an addition, a multiplication or a power math question.*/
    public String createMulti() {
        int qType = this.r.nextInt(3);
        String qString = "What is ";
        int firstNum = this.r.nextInt(12) + 2;
        int secondNum = this.r.nextInt(12) + 2;
        if (firstNum + secondNum <= 0 || firstNum * secondNum <= 0 || Math.pow(firstNum, secondNum) <= 0) {
            secondNum = this.r.nextInt(7) + 1;
            firstNum = this.r.nextInt(11) + 2;
        }
        int qAnswer = 0;
        if (qType == 0) {
            qString += firstNum + " + " + secondNum;
            qAnswer = firstNum + secondNum;
        } else if (qType == 1) {
            qString += firstNum + " * " + secondNum;
            qAnswer = firstNum * secondNum;
        } else if (qType == 2) {
            qString += firstNum + " to the power of " + secondNum;
            qAnswer = (int)Math.pow(firstNum, secondNum);
        }
        qString += ";M;" + randomiseAnswerOrder(firstNum, qAnswer);
        return qString;
    }

    //This method creates a random number between 0 and 2 and based on that number selects a question type to return (Multiple choice / short answer / true false).
    public String selectRandomQuestion() {
        int qType = this.r.nextInt(3);
        String question = "";
        if (qType == 0) {
            question = createMulti();
        } else if (qType == 1) {
            question = createShortAns();
        } else if (qType == 2) {
            question = createTF();
        }
        return question;
    }

    //This method uses the generateUniqueAnswers method to generate 4 unique values that are put into a string and returned based on a randomizer.
    public String randomiseAnswerOrder(int firstNum , int qAns) {
        int qAnsPlace = this.r.nextInt(4);
        String output = "";
        int[] ansArr = generateUniqueAnswers(qAns, firstNum);
        int first = ansArr[0];
        int second = ansArr[1];
        int third = ansArr[2];
        int forth = ansArr[3];
        if (qAnsPlace == 0) {
            output = qAns + ";" + second + ";" + third + ";" + forth + ";" + "a";
        } else if (qAnsPlace == 1) {
            output = first + ";" + qAns + ";" + third + ";" + forth + ";" + "b";
        } else if (qAnsPlace == 2) {
            output = first + ";" + second + ";" + qAns + ";" + forth + ";" + "c";
        } else if (qAnsPlace == 3) {
            output = first + ";" + second + ";" + third + ";" + qAns + ";" + "d";
        }
        return output;
    }

    //This method generates an int array that has 4 unique values that are not equal to the answer of the question.
    public int[] generateUniqueAnswers(int qAns , int firstNum) {
        int[] output = {0,0,0,0};
        boolean shouldContinue = true;
        boolean isNumberEqual = false;
        output[0] = generateBiggerThan0(qAns, firstNum);
        while (output[0] == qAns) {
            output[0] = generateBiggerThan0(qAns, firstNum);
        }
        for (int i = 1; i < output.length; i++) {
            shouldContinue = true;
            while (shouldContinue == true) {
                output[i] = generateBiggerThan0(qAns, firstNum);
                isNumberEqual = false;
                for (int j = 0; j < i; j++) {
                    if (output[j] == output[i] || output[i] == qAns) {
                        isNumberEqual = true;
                    }
                }
                if (isNumberEqual == true) {
                    shouldContinue = true;
                } else if (isNumberEqual == false) {
                    shouldContinue = false;
                }
            }
        }
        return output;
    }

    //This method generates a random number that is bigger than 0 based on two arguments.
    public int generateBiggerThan0(int qAns , int firstNum) {
        int randValue = 0;
        do {
            randValue = qAns + this.r.nextInt(firstNum) - this.r.nextInt(firstNum);
        } while (randValue <= 0);
        return this.r.nextInt(randValue) + 1;
    }
}
