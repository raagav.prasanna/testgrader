// Shay Alex Lelichev 2043812
package fileGenerators;

import java.nio.file.*;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.io.IOException;

public class StudentGenerator {

    private Path pFF;
    private Path pTF;
    private Random r;

    //sets the paths of the files that are read and generated.
    public StudentGenerator() {
        
        this.pFF = Paths.get("txtfiles/questions.txt");
        this.pTF = Paths.get("txtfiles/student.txt");
        this.r = new Random();
    }
    
    //This method writes to a file the students that were generated using generateRandomId , selectRandomName and generateStudentAnswers.
    public void generateStudents() {
        List<String> str = new ArrayList<String>();
        String strToadd = null;
        for (int i = 0; i < r.nextInt(15) + 6; i++) {
            strToadd = generateRandomId() + ";" + selectRandomName() + ";" + generateStudentAnswers();
            str.add(strToadd.substring(0 , strToadd.length() - 1));
        }
        try {
            Files.write(this.pTF , str);
        } catch (IOException e) {
            System.out.println("Something went wrong with the students file.");
        }
    }

    //This method generates a random 6 digit long id
    public String generateRandomId() {
        String id = "";
        final int idLength = 6;
        for (int i = 0; i < idLength; i++) {
            id += this.r.nextInt(10);
        }
        return id;
    }

    //This method reads from the studentNames.txt file to find a random student name.
    public String selectRandomName() {
        List<String> rn = null;
        try {
            rn = Files.readAllLines(Paths.get("fileGenerators/studentNames.txt"));
        } catch (IOException e) {
            System.out.println("There was an error with the student name number.");
        }
        return rn.get(this.r.nextInt(60));
    }

    //This method checks each line in the questions.txt file to find the type of question and randomize a student answer.
    public String generateStudentAnswers() {
        List<String> gSA = null;
        String[] str = null;
        String strP = null;
        String returnS = "";
        int randomV = 0;
        final int gSASize = 10;
        try {
            gSA = Files.readAllLines(this.pFF);
        } catch (IOException e) {
            System.out.println("There was an error with the questions file.");
        }
        for (int i = 0; i < gSASize; i++) {
            strP = gSA.get(i);
            str = strP.split(";");
            if (str[2].equals("M")) {
                randomV = this.r.nextInt(4);
                if (randomV == 0) {
                    returnS += "a;";
                } else if (randomV == 1) {
                    returnS += "b;";
                } else if (randomV == 2) {
                    returnS += "c;";
                } else if (randomV == 3) {
                    returnS += "d;";
                }
            } else if (str[2].equals("S")) {
                randomV = this.r.nextInt(2);
                if (randomV == 0) {
                    returnS += str[3] + ";";
                } else if (randomV == 1) {
                    returnS += generateRandomString();
                }
            } else if (str[2].equals("T")) {
                randomV = this.r.nextInt(2);
                if (randomV == 0) {
                    returnS += "TRUE;";
                } else if (randomV == 1) {
                    returnS += "FALSE;";
                }
            }
        }
        return returnS;
    }

    //This method generates a random string of lowercase letters that is between 1 and 14 characters long.
    public String generateRandomString() {
        int endValue;
        int startValue;
        String rString = "";
        do {
            endValue = this.r.nextInt(10) + 5;
            startValue = this.r.nextInt(10) + 1;
        } while (endValue <= startValue);
        while (startValue < endValue) {
            rString += (char)(this.r.nextInt(26) + 'a');
            startValue++;
        }
        return rString + ";";
    }
}
