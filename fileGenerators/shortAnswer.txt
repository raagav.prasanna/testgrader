How many people are in the world?;S;7.9 billion
What year was the very first model of the iPhone released?;S;2007
What does "HTTP" stand for?;S;HyperText Transfer Protocol
Which email service is owned by Microsoft?;S;Hotmail
Google Chrome, Safari, Firefox, and Explorer are different types of what?;S;Web browsers
Who discovered penicillin?;S;Alexander Fleming
What part of the atom has no electric charge?;S;Neutron
What is the symbol for potassium?;S;K
Which planet is the hottest in the solar system?;S;Venus
Which natural disaster is measured with a Richter scale?;S;Earthquakes
How many molecules of oxygen does ozone have?;S;Three
What is the name of Batman's butler?;S;Alfred
What does DC stand for?;S;Detective Comics
How many Lord of the Rings films are there?;S;Three
In which US city is Broadway located?;S;New York City
How many parts (screws and bolts included) does the average car have?;S;30000
Which boxer was known as "The Greatest" and "The People's Champion"?;S;Muhammad Ali
What is the plural of goose?;S;Geese
Which kind of alcohol is Russia notoriously known for?;S;Vodka
Which country invented tea?;S;China