// Raagav Prasanna 2036159

package javaFiles;

import java.util.List;
import java.nio.file.*;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.control.*;


public class AppAction implements EventHandler<ActionEvent> {
    private TextField msgBar;
    private TextArea response;
    private ChoiceBox avgChoice;
    private TextField questUrl;
    private TextField studsUrl;  
    
    // Constructor will initialize all input javafx objects
    public AppAction(TextField msgBar, TextArea response, ChoiceBox avgChoice, TextField questUrl, TextField studsUrl) {
        this.msgBar = msgBar;
        this.response = response;
        this.avgChoice = avgChoice;
        this.questUrl = questUrl;
        this.studsUrl = studsUrl;
    }
    @Override
    // Should execute when the submit button is pressed
    public void handle(ActionEvent ae) {
        try {
            // in the event that any exceptions are thrown, they will be caught and the message bars text will update
            FileImporter fi = new FileImporter(studsUrl.getText(), questUrl.getText());
            FileExporter fe = new FileExporter(fi.getStudentArray());
            String avg = fe.getClassroom().getClassAverage(avgChoice.getValue() +"");
            List<String> retInfo = fe.getOutputFileList();
            Files.write(Paths.get("txtfiles/outputfile.txt"), retInfo);
            this.response.setText("Data appended to file: " +retInfo + "\n" +"Class average is: " +avg);
            // In the event that no exceptions are thrown the messages bars text will be set to success
            this.msgBar.setText("Data succesfully appended to outputfile.txt");
        } catch (IllegalArgumentException e) {
            // will update the text of the message bar to the text of the Illegal argument exception thrown
            this.msgBar.setText(e.getMessage());
            this.response.setText(":(");
        }
        // Will not normally be called
        catch(Exception all) {
            // will change the text of the message bar stating that there is a run time error
            this.msgBar.setText("There was a run time error, please check console for full details: " +all.getMessage() );
            this.response.setText(":(");
            // the stack trace will then print for the developer to fix any issues
            all.printStackTrace();
        }
    }
}
