// Raagav Prasanna 2036159

package javaFiles;

import java.util.ArrayList;

public class Classroom {
    private ArrayList<Student> students;
    // constructor initializes student array that is given from the fileImporter class
    public Classroom(ArrayList<Student> inpStuds) {
        this.students = new ArrayList<>(inpStuds);
    }
    
    public String getClassAverage(String averageType) {
        String retAvg = "";
        // all conditions use the percentageGrade value which is why it is initialized here
        Double percentageGrade = calcPercentage();
        if(averageType.equals("Percentage")) {
            retAvg = percentageGrade.intValue() + "%";
        }
        else if(averageType.equals("Letter")) {
            retAvg = getLetter(percentageGrade);
        }
        else if(averageType.equals("GPA")) {
            retAvg = getGPA(percentageGrade);
        }
        else {
            // Will be thrown usually when a an averageType is not selected in the program. 
            throw new IllegalArgumentException("Invalid average type: " +averageType);
        }
        return retAvg; 
    }
    private Double calcPercentage() {
        // Am using the Double class to get access to the methods that the object has
        Double average = 0.0;
        // calculates the sum of all the grades
        for (int x : getAllStudentsGrades()) {
            average+=x;
        }
        // calculates the average as a percentage
        average = (average / (getAllStudentsGrades().size() * 10.0)) * 100.0;
        // throws an exception if the calculated average is not valid
        if(average < 0 || average > 100) {
            throw new IllegalArgumentException("The calculated average: " +average +", is invalid");
        }
        return(average);
    }
    // takes as input the calculaed numeric grade and converts it to the corresponding value in the letter grading system (A, B, C, D or F)
    private String getLetter(Double inpGrade) {
        String retGrade = "";
        if(inpGrade >= 0 && inpGrade < 65) {
            retGrade = "F";
        }
        else if(inpGrade >= 65 && inpGrade < 70) {
            retGrade = "D";
        }
        else if(inpGrade >= 70 && inpGrade < 80) {
            retGrade = "C";
        }
        else if(inpGrade >= 80 && inpGrade < 90) {
            retGrade = "B";
        }
        else if(inpGrade >= 90 && inpGrade <= 100) {
            retGrade = "A";
        }
        else {
            throw new IllegalArgumentException("Exception for invalid calculated average: " +inpGrade +", was not thrown");
        }
        return retGrade;
    }
    // takes as input the calculated numeric grade and converts it to the approximate value in the GPA grading system
    private String getGPA(Double inpGrade) {
        String retGrade = "";
        if(inpGrade >= 0 && inpGrade < 65) {
            retGrade = "0.0";
        }
        else if(inpGrade >= 65 && inpGrade < 67) {
            retGrade = "1.0";
        }
        else if(inpGrade >= 67 && inpGrade < 70) {
            retGrade = "1.3";
        }
        else if(inpGrade >= 70 && inpGrade < 73) {
            retGrade = "1.7";
        }
        else if(inpGrade >= 73 && inpGrade < 77) {
            retGrade = "2.0";
        }
        else if(inpGrade >= 77 && inpGrade < 80) {
            retGrade = "2.3";
        }
        else if(inpGrade >= 80 && inpGrade < 83) {
            retGrade = "2.7";
        }
        else if(inpGrade >= 83 && inpGrade < 87) {
            retGrade = "3.0";
        }
        else if(inpGrade >= 87 && inpGrade < 90) {
            retGrade = "3.3";
        }
        else if(inpGrade >= 90 && inpGrade < 93) {
            retGrade = "3.7";
        }
        else if(inpGrade >= 93 && inpGrade <= 100) {
            retGrade = "4.0";
        }
        else {
            throw new IllegalArgumentException("Exception for invalid calculated average: " +inpGrade +", was not thrown");
        }
        return retGrade;
    }
    public ArrayList<Student> getStudents() {
        return this.students;
    }
    // Was mainly used to print all students grades without iterating in the main method
    public ArrayList<Integer> getAllStudentsGrades() {
        ArrayList<Integer> retArr = new ArrayList<>();
        for(Student s : this.students) {
            retArr.add(s.getGrade());
        }
        return retArr;
    }
    
}