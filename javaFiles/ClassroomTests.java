// Shay Alex Lelichev 2043812
package javaFiles;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ClassroomTests {

    @Test
    public void testConstruct() {
        
        try {
            Classroom cr = new Classroom(new ArrayList<Student>());
        } catch (Exception e) {
            fail("Wrong format for students.");
        }
    }

    @Test
    public void testGetClassAverage() {
        Classroom cr = new Classroom(new ArrayList<Student>());
        assertEquals("", cr.getClassAverage("Percent"));
    }

    @Test
    public void testToString() {
        Classroom cr = new Classroom(new ArrayList<Student>());
        assertEquals("", cr.toString());
    } 
}