// Raagav Prasanna 2036159

package javaFiles;
import java.util.ArrayList;

public class Exam {
    
    private ArrayList<Question> questions;
    
    // Takes as input the Question array list given by the FileImporter class
    public Exam(ArrayList<Question> inpQuests) {
        this.questions = new ArrayList<>(inpQuests);
    }
    
    public ArrayList<Question> getQuestions() {
        return this.questions;
    }
    
    // returns the Question object that corresponds to the inputted questionId String
    public Question getQuestion(String questionId) {
        for(Question q : this.questions) {
            if(q.getQuestionId().equals(questionId)) {
                return q;
            }
        }
        // throws in exception in the event that the questionId doesn't exist
        throw new IllegalArgumentException("Question not found");
    }
}
