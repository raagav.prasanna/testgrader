// Shay Alex Lelichev 2043812
package javaFiles;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ExamTests {

    @Test
    public void testConstruct() {
        ArrayList<Question> q = new ArrayList<Question>();
        try {
            Exam ex = new Exam(q);
        } catch (Exception e) {
            fail("Wrong parameters for the constructor");
        }
    }

    @Test
    public void testGetQuestions() {
        ArrayList<Question> q = new ArrayList<Question>();
        Exam ex = new Exam(q);
        assertEquals(q, ex.getQuestions());
    }

    @Test
    public void testGetQuestion() {
        ArrayList<Question> q = new ArrayList<Question>();
        Question q1 = new ShortAnswer("0001" , "test" , "a");
        q.add(q1);
        Exam ex = new Exam(q);
        assertEquals(q1, ex.getQuestion("0001"));
    }
}