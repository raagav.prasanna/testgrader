// Raagav Prasanna 2036159 

package javaFiles;
import java.util.ArrayList;


public class FileExporter {
      private Classroom classroom;

      // Takes as input the students array list given by the FileImporter class
      public FileExporter(ArrayList<Student> inpStudents) {
          this.classroom = new Classroom(inpStudents);
      }

      // returns the list that will be outputed to the file.
      public ArrayList<String> getOutputFileList() {
        ArrayList<String> retStuds = new ArrayList<>();
        for (Student s : this.classroom.getStudents()) {
          // adds the returned formatted string to the list
          retStuds.add(getStudString(s));
        }
        return retStuds;
      }

      public Classroom getClassroom() {
        return this.classroom;
      }

      // gets each individual line for the list.
      private String getStudString(Student inpStud) {
        String retStr = inpStud.getStudId() +";" +inpStud.getStudName() +";" +inpStud.getGrade() +"/" +inpStud.getStudExam().getQuestions().size();
        return retStr;
      }
}
