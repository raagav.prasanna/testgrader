// Raagav Prasanna 2036159

package javaFiles;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.*;

public class FileImporter{
    
    private ArrayList<Student> studs;
    private ArrayList<Question> questions;
    
    public FileImporter(String answersFile, String questionsFile) throws IOException {
        // initializes the value of the Question array list
        this.questions = new ArrayList<>(fetchQuestions(questionsFile));
        // initilaizes the value of the Student array list
        this.studs = new ArrayList<>(fetchStudents(answersFile, this.questions));
    }
    
    public ArrayList<Student> getStudentArray() {
        
        return this.studs;
    }
    
    public ArrayList<Question> getQuestionArray() {
        
        return this.questions;
    }
   
    // returns the array list students from the inputted file
    private ArrayList<Student> fetchStudents(String inpFileUrl, ArrayList<Question> inpExam) {
        
        List<String> fileLines = new ArrayList<>();
        try {
            // will throw an excpetion in the event that the file doesn't exist
            Path p = Paths.get(inpFileUrl);
            fileLines = Files.readAllLines(p);
        }
        // catches the exception and throws another exception that will be caught to change the text in the msgBar
        catch(IOException e) {
            throw new IllegalArgumentException(inpFileUrl +" does not exist");
        }
        ArrayList<String> inpArr = new ArrayList<>(fileLines);
        if(inpArr.size() == 0 || inpArr.get(0).chars().filter(ch -> ch == ';').count() < 3) {
            throw new IllegalArgumentException("Invalid file format for: " +inpFileUrl);
        }
        ArrayList<Student> retArr= new ArrayList<>();
        // sets the value of the return array to the parsed returned student array
        retArr = parseStudents(inpArr, inpExam);
        return retArr ;
    }
    
    //  returns the parsed student array
    private ArrayList<Student> parseStudents(ArrayList<String> inpArr, ArrayList<Question> inpExam) {
        
        String[][] parsedInfo = new String[inpArr.size()][];
        ArrayList<Student> retArr = new ArrayList<>();
        for (int i = 0; i < parsedInfo.length; i++) {
            parsedInfo[i] = inpArr.get(i).split(";");
            // appends each student object to the array list of students
            retArr.add(retStudObj(parsedInfo[i], inpExam));
            //testPrint(parsedInfo[i]);
        }
        return retArr;
    }
    
    // returns a student object
    private Student retStudObj(String[] inpArr, ArrayList<Question> inpExam) {
        
        // creates the student object and then returns it
        Student retStud = new Student(inpArr, inpExam);
        return retStud;
    }
    
    // returns the array list questions from the inputted file
    protected ArrayList<Question> fetchQuestions(String inpFileUrl) throws IOException{
        
        List<String> fileLines = new ArrayList<>();
        try {
            // will throw an excpetion in the event that the file doesn't exist
            Path p = Paths.get(inpFileUrl);
            fileLines = Files.readAllLines(p);
        }
        // catches the exception and throws another exception that will be caught to change the text in the msgBar
        catch(IOException e) {
            throw new IllegalArgumentException(inpFileUrl +" does not exist");
        }
        ArrayList<String> inpArr = new ArrayList<>(fileLines);
        if(inpArr.size() != 10) {
            throw new IllegalArgumentException("questions file must contain 10 questions");
        }
        ArrayList<Question> retArr= new ArrayList<>();
        // sets the value of the return array to the parsed returned question array
        retArr = parseQuestions(inpArr);
        return retArr;
    }
    
    private ArrayList<Question> parseQuestions(ArrayList<String> inpArr) {
        
        String[][] parsedInfo = new String[inpArr.size()][];
        ArrayList<Question> retArr = new ArrayList<>();
        for(int i =0; i< parsedInfo.length; i++) {
            parsedInfo[i] = inpArr.get(i).split(";");
            // appends each returned question object to the question array list
            retArr.add(retQuestObj(parsedInfo[i]));
            //testPrint(parsedInfo[i]);
        }
        return retArr;
    }
    
    // testing method that was used to print the questions
    public void testPrint(String[] inpArr) {
        
        for(String s: inpArr) {
            System.out.print(s + ":");
        }
        System.out.println("");
    }
    
    // returns the question object depending on the format of the line in the txt file
    private Question retQuestObj(String[] inpArr) {
        
        final int TYPE_INDEX = 2;
        final int NAME_INDEX = 1;
        final int ANSWER_INDEX = 3;
        final int ID_INDEX = 0;
        if(inpArr.length != 8 && inpArr.length != 4) {
            throw new IllegalArgumentException("Invalid format for a line in questions file");
        }

        if (inpArr[TYPE_INDEX].equals("M")) {
            return (new MultipleChoice(inpArr[ID_INDEX], inpArr[NAME_INDEX], inpArr[inpArr.length - 1], fetchOptions(inpArr)));
        }
        else if (inpArr[TYPE_INDEX].equals("T")) {
            return (new TrueFalse(inpArr[ID_INDEX], inpArr[NAME_INDEX], inpArr[ANSWER_INDEX]));
        }
        else if (inpArr[TYPE_INDEX].equals("S")) {
            return (new ShortAnswer(inpArr[ID_INDEX], inpArr[NAME_INDEX], inpArr[ANSWER_INDEX ]));
        }
        else {
            throw new IllegalArgumentException("Invalid question type in input file!");
        }
    }
    
    private String[] fetchOptions(String[] inpArr) {
        
        return(new String[]{inpArr[3], inpArr[4], inpArr[5], inpArr[6]});
    }
    
    public String toString() {
        
        String retStr = "";
        for(Question q : this.questions) {
            retStr += q + "\n";
        }
        return retStr;
    }
}