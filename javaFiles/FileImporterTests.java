// Shay Alex Lelichev 2043812
package javaFiles;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.Test;

public class FileImporterTests {
    
    @Test
    public void testFetchQ() {

        try {
            FileImporter fi = new FileImporter("C:\\Users\\raaga\\Desktop\\DawsonClasses\\ComputerScience\\3rd_Semester\\Programming_3\\FinalProject\\testgrader\\txtfiles\\test.txt", 
            "C:\\Users\\raaga\\Desktop\\DawsonClasses\\ComputerScience\\3rd_Semester\\Programming_3\\FinalProject\\testgrader\\txtfiles\\sample_questions.txt");
            if (fi.getQuestionArray().isEmpty()) {
                fail("The question file is incorrect.");
            } else {
                System.out.println(fi.getQuestionArray());
            }
        } catch (IOException e) {
            fail("One of the parameters for the constructor is wrong");
        }
    }
}
