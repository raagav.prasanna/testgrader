// Raagav Prasanna 2036159

package javaFiles;

import java.util.Iterator;
import java.util.HashMap;

public class MultipleChoice extends Question{
    // index respresents option (a, b, c, d) and value represents the corresponding answer
    private HashMap<String, String> questionResponses;

    public MultipleChoice(String questionId , String questionText , String questionAnswer, String[] choices) {
        if(choices.length != 4) {
            throw new IllegalArgumentException("Number of answer choices must be equal to 4");
        }
        String[] temp = new String[]{"a","b","c","d"};
        this.questionResponses = new HashMap<String, String>();
        this.questionId = questionId;
        this.questionType = "Multiple Choice";
        this.questionText = questionText;
        this.questionAnswer = questionAnswer;
        for(int i =0; i<choices.length; i++) {
            this.questionResponses.put(temp[i], choices[i]);
        }
    }

    public String getQuestionId() {
        return this.questionId;
    }
    
    public String getQuestionType() {
        return this.questionType;
    }
    
    public String getQuestionText() {
        return this.questionText;
    }
    
    public String getQuestionAnswer() {
        return this.questionAnswer;
    }

    public HashMap<String,String> getQuestionChoices() {
        return this.questionResponses;
    }
    
    public String toString() {
        String retStr =  "Question Id: " + this.questionId + " Question: " +this.questionText +" Question Type: " +this.questionType +" Question options: " +"\n";
        Iterator it = this.questionResponses.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            retStr += (pair.getKey() + " : " + pair.getValue() +"\n");
            it.remove(); // avoids a ConcurrentModificationException
        }
        retStr += "Question Answer: " +this.questionAnswer +"\n";
        return retStr;
    }
}
