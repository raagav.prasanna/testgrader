// Shay Alex Lelichev 2043812
package javaFiles;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import java.util.HashMap;

public class MultipleChoiceTests {

    @Test
    public void testConstruct() {
        try {
            String [] qc = {"1" , "2" , "3" , "4"};
            Question q1 = new MultipleChoice("0001", "test", "a", qc);
        } catch (Exception e) {
            fail("The constructor has incorrect parameters.");
        }
    }

    @Test
    public void testGetQuestionId() {
        String [] qc = {"1" , "2" , "3" , "4"};
        Question q1 = new MultipleChoice("0001", "test", "a", qc);
        assertEquals("0001", q1.getQuestionId());
    }

    @Test
    public void testGetQuestionType() {
        String [] qc = {"1" , "2" , "3" , "4"};
        Question q1 = new MultipleChoice("0001", "test", "a", qc);
        assertEquals("Multiple Choice", q1.getQuestionType());
    }

    @Test
    public void testGetQuestionText() {
        String [] qc = {"1" , "2" , "3" , "4"};
        Question q1 = new MultipleChoice("0001", "test", "a", qc);
        assertEquals("test", q1.getQuestionText());
    }

    @Test
    public void testGetQuestionAnswer() {
        String [] qc = {"1" , "2" , "3" , "4"};
        Question q1 = new MultipleChoice("0001", "test", "a", qc);
        assertEquals("a", q1.getQuestionAnswer());
    }

    @Test
    public void testGetQuestionChoices() {
        String [] qc = {"1" , "2" , "3" , "4"};
        MultipleChoice q1 = new MultipleChoice("0001", "test", "a", qc);
        HashMap<String , String> r = new HashMap<String,String>();
        String[] temp = new String[]{"a","b","c","d"};
        for (int i = 0; i < qc.length; i++) {
            r.put(temp[i] , qc[i]);
        }
        assertEquals(r, q1.getQuestionChoices());
    }
}