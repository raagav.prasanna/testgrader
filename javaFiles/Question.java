// Raagav Prasanna 2036159

package javaFiles;
public abstract class Question {
    protected String questionId;
    protected String questionType;
    protected String questionText;
    protected String questionAnswer;

    public abstract String getQuestionId();
    public abstract String getQuestionType();
    public abstract String getQuestionText();
    public abstract String getQuestionAnswer();
}