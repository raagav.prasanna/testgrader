// Raagav Prasanna 2036159

package javaFiles;
public class ShortAnswer extends Question {
    
    public ShortAnswer(String questionId , String questionText , String questionAnswer) {
        this.questionId = questionId;
        this.questionType = "Short Answer";
        this.questionText = questionText;
        this.questionAnswer = questionAnswer;
    }

    public String getQuestionId() {
        return this.questionId;
    }
    
    public String getQuestionType() {
        return this.questionType;
    }
    
    public String getQuestionText() {
        return this.questionText;
    }
    
    public String getQuestionAnswer() {
        return this.questionAnswer;
    }
    
    public String toString() {
        return ("Question Id: " + this.questionId + " Question: " +this.questionText +" Question Type: " +this.questionType +" Question Answer: " +this.questionAnswer +"\n");
    } 
}
