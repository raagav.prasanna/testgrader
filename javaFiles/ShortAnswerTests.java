// Shay Alex Lelichev 2043812
package javaFiles;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ShortAnswerTests {

    @Test
    public void testConstruct() {
        try {
            Question q1 = new ShortAnswer("0001", "test", "a");
        } catch (Exception e) {
            fail("The parameters for the constructor are wrong.");
        }
    }

    @Test
    public void testGetQuestionId() {
        Question q1 = new ShortAnswer("0001", "test", "a");
        assertEquals("0001", q1.getQuestionId());
    }

    @Test
    public void testGetQuestionType() {
        Question q1 = new ShortAnswer("0001", "test", "a");
        assertEquals("Short Answer", q1.getQuestionType());
    }

    @Test
    public void testGetQuestionText() {
        Question q1 = new ShortAnswer("0001", "test", "a");
        assertEquals("test", q1.getQuestionText());
    }

    @Test
    public void testGetQuestionAnswer() {
        Question q1 = new ShortAnswer("0001", "test", "a");
        assertEquals("a", q1.getQuestionAnswer());
    }
}