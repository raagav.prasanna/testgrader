// Raagav Prasanna 2036159

package javaFiles;

import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;

public class Student {
    
    private String studId;
    private String studName;
    private HashMap<String, String> responses;
    private int grade;
    private Exam studExam;
    
    public Student(String[] studInfo, ArrayList<Question> questions) {
        // if input array doesn't equal 12 it means that the input file format was invalid
        if(studInfo.length != 12) {
            throw new IllegalArgumentException("Stud contructor only takes as input an array with the studId, name, and 10 questions");
        }
        this.studExam = new Exam(questions);
        this.studId = studInfo[0];
        this.studName = studInfo[1];
        this.responses = new HashMap<>();
        for (int i = 0; i < this.studExam.getQuestions().size(); i++) {
            // initalizes the HashMap which has the question id as its key and the students response for that question as the value
            this.responses.put(this.studExam.getQuestions().get(i).getQuestionId(), studInfo[i + 2]);
        }
        this.grade = calcGrade();
    }
   
    public String getStudId() {
        return this.studId;
    }
    
    public String getStudName() {
        return this.studName;
    }
    
    public int getGrade() {
        return this.grade;
    }
    public Exam getStudExam() {
        return this.studExam;
    }
    // checks how many questions the student got right and returns that count
    public int calcGrade() {
        int retGrade = 0;
        String key = "";
        String val = "";
        Iterator it = this.responses.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            key = pair.getKey() + "";
            val = pair.getValue() + "";
            if( val.equals( this.studExam.getQuestion(key).getQuestionAnswer() ) ) {
                retGrade++;
            }
            it.remove();
        }
        return retGrade;
    }
    
    public String toString() {
        String retStr = "ID: " +this.studId +" Name: " +this.studName +"\n" +"Exam Responses: " +"\n";
        Iterator i = this.responses.entrySet().iterator();
        while (i.hasNext()) {
            HashMap.Entry set = (HashMap.Entry)i.next();
            retStr += (set.getKey() + " : " + set.getValue() +"\n");
            i.remove();
        }
        return retStr;
    }
    public HashMap<String,String> getResponses() {
        return this.responses;
    }
}