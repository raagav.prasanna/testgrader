// Shay Alex Lelichev 2043812
package javaFiles;

import java.util.ArrayList;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;


public class StudentTests {

    @Test
    public void testConstruct() {
        try {    
            String[] si = {"203726","Raagav","a","FALSE","TRUE","A=sk","geese","b","c","d","a","a"};
            ArrayList<Question> q = new ArrayList<Question>();
            Question q1 = new ShortAnswer("0001" , "test" , "a");
            q.add(q1);
            Student s1 = new Student(si, q);
        } catch (Exception e) {
            fail("The parameters for the constructor are incorrect.");
        }
    }

    @Test
    public void testGetStudId() {
        String[] si = {"203726","Raagav","a","FALSE","TRUE","A=sk","geese","b","c","d","a","a"};
        ArrayList<Question> q = new ArrayList<Question>();
        Question q1 = new ShortAnswer("0001" , "test" , "a");
        q.add(q1);
        Student s1 = new Student(si, q);
        assertEquals("203726", s1.getStudId());
    }

    @Test
    public void testGetStudName() {
        String[] si = {"203726","Raagav","a","FALSE","TRUE","A=sk","geese","b","c","d","a","a"};
        ArrayList<Question> q = new ArrayList<Question>();
        Question q1 = new ShortAnswer("0001" , "test" , "a");
        q.add(q1);
        Student s1 = new Student(si, q);
        assertEquals("Raagav", s1.getStudName());
    }

    @Test
    public void testGetGrade() {
        String[] si = {"203726","Raagav","a","FALSE","TRUE","A=sk","geese","b","c","d","a","a"};
        ArrayList<Question> q = new ArrayList<Question>();
        Question q1 = new ShortAnswer("0001" , "test" , "a");
        q.add(q1);
        Student s1 = new Student(si, q);
        assertEquals(0, s1.getGrade());
    }

    @Test
    public void testCalcGrade() {
        String[] si = {"203726","Raagav","a","FALSE","TRUE","A=sk","geese","b","c","d","a","a"};
        ArrayList<Question> q = new ArrayList<Question>();
        Question q1 = new ShortAnswer("0001" , "test" , "a");
        q.add(q1);
        Student s1 = new Student(si, q);
        assertEquals(0, s1.calcGrade());
    }
}