//Raagav Prasanna 2036159

package javaFiles;
import java.io.IOException;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class TestGraderApplication extends Application{
        public void start(Stage stage) {
            // GUI for application
            Group root = new Group();
            Scene scene = new Scene(root, 650, 300);
            scene.setFill(Color.BEIGE);
            stage.setTitle("Test Grader");
            stage.setScene(scene);
            stage.show();

            VBox content = new VBox();
            TextField msgBar = new TextField("Welcome to The test Grader application");
            HBox pathEntryRow = new HBox();
            TextField questInp = new TextField("--Please enter path of file with questions here--");
            TextField studsInp = new TextField("--Please enter path of file with studs here--");
            questInp.setPrefWidth(500);
            studsInp.setPrefWidth(500);
            pathEntryRow.getChildren().addAll(questInp, studsInp);
            ChoiceBox averageOptions = new ChoiceBox();
            Text avgMsg = new Text("Select what class average type you would like the program to return");
            Text instructions = new Text("Please check the instructions file for details on how to use the application");
            averageOptions.getItems().add("Percentage");
            averageOptions.getItems().add("Letter");
            averageOptions.getItems().add("GPA");
            Button submitBtn = new Button("submit");
            TextArea response = new TextArea();
            // button calls the handle in the AppAction class
            submitBtn.setOnAction(new AppAction(msgBar, response, averageOptions, questInp, studsInp));
            content.getChildren().addAll(msgBar,pathEntryRow,avgMsg,averageOptions,submitBtn, response, instructions);  
            root.getChildren().add(content);
        }
    public static void main(String[] args) throws IOException{
        // launches the GUI
        Application.launch(args);
    }
}