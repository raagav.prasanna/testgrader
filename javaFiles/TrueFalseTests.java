// Shay Alex Lelichev 2043812
package javaFiles;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TrueFalseTests {

    @Test
    public void testConstruct() {
        try {
            Question q1 = new TrueFalse("0001", "test", "a");
        } catch (Exception e) {
            fail("The constructor has wrong parameters.");
        }
    }

    @Test
    public void testGetQuestionId() {
        Question q1 = new TrueFalse("0001", "test", "a");
        assertEquals("0001", q1.getQuestionId());
    }

    @Test
    public void testGetQuestionType() {
        Question q1 = new TrueFalse("0001", "test", "a");
        assertEquals("True or False", q1.getQuestionType());
    }

    @Test
    public void testGetQuestionText() {
        Question q1 = new TrueFalse("0001", "test", "a");
        assertEquals("test", q1.getQuestionText());
    }

    @Test
    public void testGetQuestionAnswer() {
        Question q1 = new TrueFalse("0001", "test", "a");
        assertEquals("a", q1.getQuestionAnswer());
    }
}